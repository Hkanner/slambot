// g2o - General Graph Optimization
// Copyright (C) 2011 H. Strasdat
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef G2O_SE3QUATR3VB_H_
#define G2O_SE3QUATR3VB_H_

#include "se3_ops.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace g2o {
  using namespace Eigen;

  typedef Matrix<double, 6, 1> Vector6d;
  typedef Matrix<double, 7, 1> Vector7d;
  typedef Matrix<double,15,1> Vector15d;
  typedef Matrix<double,16,1> Vector16d;

  class SE3QuatR3VB {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;


    protected:

      Quaterniond _r;
      Vector3d _t;
      Vector3d _v;
      Vector3d _bg;
      Vector3d _ba;


    public:
      SE3QuatR3VB(){
        _r.setIdentity();
        _t.setZero();
        _v.setZero();
        _bg.setZero();
        _ba.setZero();
      }

      SE3QuatR3VB(const Matrix3d& R, const Vector3d& t, const Vector3d& v, const Vector3d& bg, const Vector3d& ba):_r(Quaterniond(R)),_t(t),_v(v),_bg(bg),_ba(ba){ 
        normalizeRotation();
      }

      SE3QuatR3VB(const Quaterniond& q, const Vector3d& t, const Vector3d& v, const Vector3d& bg, const Vector3d& ba):_r(q),_t(t),_v(v),_bg(bg),_ba(ba){
        normalizeRotation();
      }

      /**
       * templaized constructor which allows v to be an arbitrary Eigen Vector type, e.g., Vector6d or Map<Vector6d>
       */
      //TODO: change this to incorporate full state
      template <typename Derived>
        explicit SE3QuatR3VB(const MatrixBase<Derived>& v)
        {
         assert((v.size() == 6 || v.size() == 7) && "Vector dimension does not match");
          if (v.size() == 6) {
            for (int i=0; i<3; i++){
              _t[i]=v[i];
              _r.coeffs()(i)=v[i+3];
            }
            _r.w() = 0.; // recover the positive w
            if (_r.norm()>1.){
              _r.normalize();
            } else {
              double w2=1.-_r.squaredNorm();
              _r.w()= (w2<0.) ? 0. : sqrt(w2);
            }
          }
          else if (v.size() == 7) {
            int idx = 0;
            for (int i=0; i<3; ++i, ++idx)
              _t(i) = v(idx);
            for (int i=0; i<4; ++i, ++idx)
              _r.coeffs()(i) = v(idx);
            normalizeRotation();
          }
        }

      inline const Vector3d& translation() const {return _t;}

      inline void setTranslation(const Vector3d& t_) {_t = t_;}

      inline const Quaterniond& rotation() const {return _r;}

      void setRotation(const Quaterniond& r_) {_r=r_;}

      inline const Vector3d& velocity() const {return _v;}

      inline void setVelocity(const Vector3d& v_) {_v = v_;}

      inline const Vector3d& gyroBias() const {return _bg;}

      inline void setgyroBias(const Vector3d& bg_) {_bg = bg_;}

      inline const Vector3d& accelBias() const {return _ba;}

      inline void setaccelBias(const Vector3d& ba_) {_ba = ba_;}

      inline SE3QuatR3VB operator* (const SE3QuatR3VB& tr2) const{
        SE3QuatR3VB result(*this);
        result._t += _r*tr2._t;
        result._r*=tr2._r;
        result._v += tr2._v;
        result._bg += tr2._bg;
        result._ba += tr2._ba;
        result.normalizeRotation();
        return result;
      }

      inline SE3QuatR3VB& operator*= (const SE3QuatR3VB& tr2){
        _t+=_r*tr2._t;
        _r*=tr2._r;
        _v += tr2._v;
        _bg += tr2._bg;
        _ba += tr2._ba;
        normalizeRotation();
        return *this;
      }

      inline Vector3d operator* (const Vector3d& v) const {
        return _t+_r*v;
      }

      inline SE3QuatR3VB inverse() const{
        SE3QuatR3VB ret;
        ret._r=_r.conjugate();
        ret._t=ret._r*(_t*-1.);
        // check conjugate() and inverse of velocity
        return ret;
      }

      inline double operator [](int i) const {
        assert(i<15);
        if (i<3)
          return _t[i];
        if(i>5 && i<9)
          return _v[i];
        if(i>8 && i<12)
          return _bg[i];
        if(i>11 && i<15)
          return _ba[i]; 
        return _r.coeffs()[i-3];
      }


      inline Vector16d toVector() const{
        Vector16d v;
        v[0]=_t(0);
        v[1]=_t(1);
        v[2]=_t(2);
        v[3]=_r.x();
        v[4]=_r.y();
        v[5]=_r.z();
        v[6]=_r.w();
        v[7]=_v(0);
        v[8]=_v(1);
        v[9]=_v(2);
        v[10]=_bg(0);
        v[11]=_bg(1);
        v[12]=_bg(2);
        v[13]=_ba(0);
        v[14]=_ba(1);
        v[15]=_ba(2);
        return v;
      }

      inline void fromVector(const Vector16d& v){
        _r=Quaterniond(v[6], v[3], v[4], v[5]);
        _t=Vector3d(v[0], v[1], v[2]);
        _v=Vector3d(v[7],v[8],v[9]);
        _bg=Vector3d(v[10],v[11],v[12]);
        _ba=Vector3d(v[13],v[14],v[15]);
      }

      inline Vector15d toMinimalVector() const{
        Vector15d v;
        v[0]=_t(0);
        v[1]=_t(1);
        v[2]=_t(2);
        v[3]=_r.x();
        v[4]=_r.y();
        v[5]=_r.z();
        v[6]=_v(0);
        v[7]=_v(1);
        v[8]=_v(2);
        v[9]=_bg(0);
        v[10]=_bg(1);
        v[11]=_bg(2);
        v[12]=_ba(0);
        v[13]=_ba(1);
        v[14]=_ba(2);
        return v;
      }

      inline void fromMinimalVector(const Vector15d& v){
        double w = 1.-v[3]*v[3]-v[4]*v[4]-v[5]*v[5];
        if (w>0){
          _r=Quaterniond(sqrt(w), v[3], v[4], v[5]);
        } else {
          _r=Quaterniond(0, -v[3], -v[4], -v[5]);
        }
        _t=Vector3d(v[0], v[1], v[2]);
        _v=Vector3d(v[6],v[7],v[8]);
        _bg=Vector3d(v[9],v[10],v[11]);
        _ba=Vector3d(v[12],v[13],v[14]);

      }



      Vector6d log() const {
        Vector6d res;
        Matrix3d _R = _r.toRotationMatrix();
        double d =  0.5*(_R(0,0)+_R(1,1)+_R(2,2)-1);
        Vector3d omega;
        Vector3d upsilon;


        Vector3d dR = deltaR(_R);
        Matrix3d V_inv;

        if (d>0.99999)
        {

          omega=0.5*dR;
          Matrix3d Omega = skew(omega);
          V_inv = Matrix3d::Identity()- 0.5*Omega + (1./12.)*(Omega*Omega);
        }
        else
        {
          double theta = acos(d);
          omega = theta/(2*sqrt(1-d*d))*dR;
          Matrix3d Omega = skew(omega);
          V_inv = ( Matrix3d::Identity() - 0.5*Omega
              + ( 1-theta/(2*tan(theta/2)))/(theta*theta)*(Omega*Omega) );
        }

        upsilon = V_inv*_t;
        for (int i=0; i<3;i++){
          res[i]=omega[i];
        }
        for (int i=0; i<3;i++){
          res[i+3]=upsilon[i];
        }

        return res;

      }

      Vector3d map(const Vector3d & xyz) const
      {
        Matrix3d Rcb;
        Vector3d Pcb;
        Rcb << 0.0148655429818,-0.999880929698,0.00414029679422,
               0.999557249008,0.0149672133247,0.025715529948,
               -0.0257744366974,0.00375618835797,0.999660727178;

        Pcb << -0.0216401454975,
               -0.064676986768,
               0.00981073058949;

        return Rcb*_r*(xyz - _t) + Pcb;
      }


      static SE3QuatR3VB exp(const Vector15d & update)
      {
        Vector3d omega;
        for (int i=0; i<3; i++)
          omega[i]=update[i];
        Vector3d upsilon;
        for (int i=0; i<3; i++)
          upsilon[i]=update[i+3];
        Vector3d alpha;
        for (int i=0; i<3; i++)
          alpha[i]=update[i+6];
        Vector3d beta1;
        for (int i=0; i<3; i++)
          beta1[i]=update[i+9];
        Vector3d beta2;
        for (int i=0; i<3; i++)
          beta2[i]=update[i+12];

        double theta = omega.norm();
        Matrix3d Omega = skew(omega);

        Matrix3d R;
        Matrix3d V;
        if (theta<0.00001)
        {
          //TODO: CHECK WHETHER THIS IS CORRECT!!!
          R = (Matrix3d::Identity() + Omega + Omega*Omega);

          V = R;
        }
        else
        {
          Matrix3d Omega2 = Omega*Omega;

          R = (Matrix3d::Identity()
              + sin(theta)/theta *Omega
              + (1-cos(theta))/(theta*theta)*Omega2);

          V = (Matrix3d::Identity()
              + (1-cos(theta))/(theta*theta)*Omega
              + (theta-sin(theta))/(pow(theta,3))*Omega2);
        }
        return SE3QuatR3VB(Quaterniond(R),V*upsilon,alpha,beta1,beta2);
      }

      Matrix<double, 6, 6> adj() const
      {
        Matrix3d R = _r.toRotationMatrix();
        Matrix<double, 6, 6> res;
        res.block(0,0,3,3) = R;
        res.block(3,3,3,3) = R;
        res.block(3,0,3,3) = skew(_t)*R;
        res.block(0,3,3,3) = Matrix3d::Zero(3,3);
        return res;
      }

      Matrix<double,4,4> to_homogeneous_matrix() const
      {
        Matrix<double,4,4> homogeneous_matrix;
        homogeneous_matrix.setIdentity();
        homogeneous_matrix.block(0,0,3,3) = _r.toRotationMatrix();
        homogeneous_matrix.col(3).head(3) = translation();

        return homogeneous_matrix;
      }

      void normalizeRotation(){
        if (_r.w()<0){
          _r.coeffs() *= -1;
        }
        _r.normalize();
      }

      /**
       * cast SE3Quat into an Eigen::Isometry3d
       */
      operator Eigen::Isometry3d() const
      {
        Eigen::Isometry3d result = (Eigen::Isometry3d) rotation();
        result.translation() = translation();
        return result;
      }
  };

  inline std::ostream& operator <<(std::ostream& out_str, const SE3QuatR3VB& se3)
  {
    out_str << se3.to_homogeneous_matrix()  << std::endl;
    return out_str;
  }

} // end namespace

#endif
