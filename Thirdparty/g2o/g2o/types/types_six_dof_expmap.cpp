// g2o - General Graph Optimization
// Copyright (C) 2011 H. Strasdat
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "types_six_dof_expmap.h"
#include <unsupported/Eigen/MatrixFunctions>
#include "../core/factory.h"
#include "../stuff/macros.h"
#include "math.h"

namespace g2o {

using namespace std;


Vector2d project2d(const Vector3d& v)  {
  Vector2d res;
  res(0) = v(0)/v(2);
  res(1) = v(1)/v(2);
  return res;
}

Vector3d unproject2d(const Vector2d& v)  {
  Vector3d res;
  res(0) = v(0);
  res(1) = v(1);
  res(2) = 1;
  return res;
}

VertexSE3Expmap::VertexSE3Expmap() : BaseVertex<6, SE3Quat>() {
}

bool VertexSE3Expmap::read(std::istream& is) {
  Vector7d est;
  for (int i=0; i<7; i++)
    is  >> est[i];
  SE3Quat cam2world;
  cam2world.fromVector(est);
  setEstimate(cam2world.inverse());
  return true;
}

bool VertexSE3Expmap::write(std::ostream& os) const {
  SE3Quat cam2world(estimate().inverse());
  for (int i=0; i<7; i++)
    os << cam2world[i] << " ";
  return os.good();
}

Vertex15SE3Expmap::Vertex15SE3Expmap() : BaseVertex<15, SE3QuatR3VB>() {
}

bool Vertex15SE3Expmap::read(std::istream& is) {
  Vector16d est;
  for (int i=0; i<16; i++)
    is  >> est[i];
  SE3QuatR3VB cam2world;
  cam2world.fromVector(est);
  setEstimate(cam2world.inverse());
  return true;
}

bool Vertex15SE3Expmap::write(std::ostream& os) const {
  SE3QuatR3VB cam2world(estimate().inverse());
  for (int i=0; i<16; i++)
    os << cam2world[i] << " ";
  return os.good();
}


EdgeSE3ProjectXYZ::EdgeSE3ProjectXYZ() : BaseBinaryEdge<2, Vector2d, VertexSBAPointXYZ, VertexSE3Expmap>() {
}

bool EdgeSE3ProjectXYZ::read(std::istream& is){
  for (int i=0; i<2; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeSE3ProjectXYZ::write(std::ostream& os) const {

  for (int i=0; i<2; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}


void EdgeSE3ProjectXYZ::linearizeOplus() {
  VertexSE3Expmap * vj = static_cast<VertexSE3Expmap *>(_vertices[1]);
  SE3Quat T(vj->estimate());
  VertexSBAPointXYZ* vi = static_cast<VertexSBAPointXYZ*>(_vertices[0]);
  Vector3d xyz = vi->estimate();
  Vector3d xyz_trans = T.map(xyz);

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double z = xyz_trans[2];
  double z_2 = z*z;

  Matrix<double,2,3> tmp;
  tmp(0,0) = fx;
  tmp(0,1) = 0;
  tmp(0,2) = -x/z*fx;

  tmp(1,0) = 0;
  tmp(1,1) = fy;
  tmp(1,2) = -y/z*fy;

  _jacobianOplusXi =  -1./z * tmp * T.rotation().toRotationMatrix();

  _jacobianOplusXj(0,0) =  x*y/z_2 *fx;
  _jacobianOplusXj(0,1) = -(1+(x*x/z_2)) *fx;
  _jacobianOplusXj(0,2) = y/z *fx;
  _jacobianOplusXj(0,3) = -1./z *fx;
  _jacobianOplusXj(0,4) = 0;
  _jacobianOplusXj(0,5) = x/z_2 *fx;

  _jacobianOplusXj(1,0) = (1+y*y/z_2) *fy;
  _jacobianOplusXj(1,1) = -x*y/z_2 *fy;
  _jacobianOplusXj(1,2) = -x/z *fy;
  _jacobianOplusXj(1,3) = 0;
  _jacobianOplusXj(1,4) = -1./z *fy;
  _jacobianOplusXj(1,5) = y/z_2 *fy;
}

Vector2d EdgeSE3ProjectXYZ::cam_project(const Vector3d & trans_xyz) const{
  Vector2d proj = project2d(trans_xyz);
  Vector2d res;
  res[0] = proj[0]*fx + cx;
  res[1] = proj[1]*fy + cy;
  return res;
}


Vector3d EdgeStereoSE3ProjectXYZ::cam_project(const Vector3d & trans_xyz, const float &bf) const{
  const float invz = 1.0f/trans_xyz[2];
  Vector3d res;
  res[0] = trans_xyz[0]*invz*fx + cx;
  res[1] = trans_xyz[1]*invz*fy + cy;
  res[2] = res[0] - bf*invz;
  return res;
}

EdgeStereoSE3ProjectXYZ::EdgeStereoSE3ProjectXYZ() : BaseBinaryEdge<3, Vector3d, VertexSBAPointXYZ, VertexSE3Expmap>() {
}

bool EdgeStereoSE3ProjectXYZ::read(std::istream& is){
  for (int i=0; i<=3; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeStereoSE3ProjectXYZ::write(std::ostream& os) const {

  for (int i=0; i<=3; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}

void EdgeStereoSE3ProjectXYZ::linearizeOplus() {
  VertexSE3Expmap * vj = static_cast<VertexSE3Expmap *>(_vertices[1]);
  SE3Quat T(vj->estimate());
  VertexSBAPointXYZ* vi = static_cast<VertexSBAPointXYZ*>(_vertices[0]);
  Vector3d xyz = vi->estimate();
  Vector3d xyz_trans = T.map(xyz);

  const Matrix3d R =  T.rotation().toRotationMatrix();

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double z = xyz_trans[2];
  double z_2 = z*z;

  _jacobianOplusXi(0,0) = -fx*R(0,0)/z+fx*x*R(2,0)/z_2;
  _jacobianOplusXi(0,1) = -fx*R(0,1)/z+fx*x*R(2,1)/z_2;
  _jacobianOplusXi(0,2) = -fx*R(0,2)/z+fx*x*R(2,2)/z_2;

  _jacobianOplusXi(1,0) = -fy*R(1,0)/z+fy*y*R(2,0)/z_2;
  _jacobianOplusXi(1,1) = -fy*R(1,1)/z+fy*y*R(2,1)/z_2;
  _jacobianOplusXi(1,2) = -fy*R(1,2)/z+fy*y*R(2,2)/z_2;

  _jacobianOplusXi(2,0) = _jacobianOplusXi(0,0)-bf*R(2,0)/z_2;
  _jacobianOplusXi(2,1) = _jacobianOplusXi(0,1)-bf*R(2,1)/z_2;
  _jacobianOplusXi(2,2) = _jacobianOplusXi(0,2)-bf*R(2,2)/z_2;

  _jacobianOplusXj(0,0) =  x*y/z_2 *fx;
  _jacobianOplusXj(0,1) = -(1+(x*x/z_2)) *fx;
  _jacobianOplusXj(0,2) = y/z *fx;
  _jacobianOplusXj(0,3) = -1./z *fx;
  _jacobianOplusXj(0,4) = 0;
  _jacobianOplusXj(0,5) = x/z_2 *fx;

  _jacobianOplusXj(1,0) = (1+y*y/z_2) *fy;
  _jacobianOplusXj(1,1) = -x*y/z_2 *fy;
  _jacobianOplusXj(1,2) = -x/z *fy;
  _jacobianOplusXj(1,3) = 0;
  _jacobianOplusXj(1,4) = -1./z *fy;
  _jacobianOplusXj(1,5) = y/z_2 *fy;

  _jacobianOplusXj(2,0) = _jacobianOplusXj(0,0)-bf*y/z_2;
  _jacobianOplusXj(2,1) = _jacobianOplusXj(0,1)+bf*x/z_2;
  _jacobianOplusXj(2,2) = _jacobianOplusXj(0,2);
  _jacobianOplusXj(2,3) = _jacobianOplusXj(0,3);
  _jacobianOplusXj(2,4) = 0;
  _jacobianOplusXj(2,5) = _jacobianOplusXj(0,5)-bf/z_2;
}


//Only Pose

bool EdgeSE3ProjectXYZOnlyPose::read(std::istream& is){
  for (int i=0; i<2; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeSE3ProjectXYZOnlyPose::write(std::ostream& os) const {

  for (int i=0; i<2; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}


void EdgeSE3ProjectXYZOnlyPose::linearizeOplus() {
  VertexSE3Expmap * vi = static_cast<VertexSE3Expmap *>(_vertices[0]);
  Vector3d xyz_trans = vi->estimate().map(Xw);

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double invz = 1.0/xyz_trans[2];
  double invz_2 = invz*invz;

  _jacobianOplusXi(0,0) =  x*y*invz_2 *fx;
  _jacobianOplusXi(0,1) = -(1+(x*x*invz_2)) *fx;
  _jacobianOplusXi(0,2) = y*invz *fx;
  _jacobianOplusXi(0,3) = -invz *fx;
  _jacobianOplusXi(0,4) = 0;
  _jacobianOplusXi(0,5) = x*invz_2 *fx;

  _jacobianOplusXi(1,0) = (1+y*y*invz_2) *fy;
  _jacobianOplusXi(1,1) = -x*y*invz_2 *fy;
  _jacobianOplusXi(1,2) = -x*invz *fy;
  _jacobianOplusXi(1,3) = 0;
  _jacobianOplusXi(1,4) = -invz *fy;
  _jacobianOplusXi(1,5) = y*invz_2 *fy;
}

Vector2d EdgeSE3ProjectXYZOnlyPose::cam_project(const Vector3d & trans_xyz) const{
  Vector2d proj = project2d(trans_xyz);
  Vector2d res;
  res[0] = proj[0]*fx + cx;
  res[1] = proj[1]*fy + cy;
  return res;
}


Vector3d EdgeStereoSE3ProjectXYZOnlyPose::cam_project(const Vector3d & trans_xyz) const{
  const float invz = 1.0f/trans_xyz[2];
  Vector3d res;
  res[0] = trans_xyz[0]*invz*fx + cx;
  res[1] = trans_xyz[1]*invz*fy + cy;
  res[2] = res[0] - bf*invz;
  return res;
}


bool EdgeStereoSE3ProjectXYZOnlyPose::read(std::istream& is){
  for (int i=0; i<=3; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeStereoSE3ProjectXYZOnlyPose::write(std::ostream& os) const {

  for (int i=0; i<=3; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}

void EdgeStereoSE3ProjectXYZOnlyPose::linearizeOplus() {
  VertexSE3Expmap * vi = static_cast<VertexSE3Expmap *>(_vertices[0]);
  Vector3d xyz_trans = vi->estimate().map(Xw);

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double invz = 1.0/xyz_trans[2];
  double invz_2 = invz*invz;

  _jacobianOplusXi(0,0) =  x*y*invz_2 *fx;
  _jacobianOplusXi(0,1) = -(1+(x*x*invz_2)) *fx;
  _jacobianOplusXi(0,2) = y*invz *fx;
  _jacobianOplusXi(0,3) = -invz *fx;
  _jacobianOplusXi(0,4) = 0;
  _jacobianOplusXi(0,5) = x*invz_2 *fx;

  _jacobianOplusXi(1,0) = (1+y*y*invz_2) *fy;
  _jacobianOplusXi(1,1) = -x*y*invz_2 *fy;
  _jacobianOplusXi(1,2) = -x*invz *fy;
  _jacobianOplusXi(1,3) = 0;
  _jacobianOplusXi(1,4) = -invz *fy;
  _jacobianOplusXi(1,5) = y*invz_2 *fy;

  _jacobianOplusXi(2,0) = _jacobianOplusXi(0,0)-bf*y*invz_2;
  _jacobianOplusXi(2,1) = _jacobianOplusXi(0,1)+bf*x*invz_2;
  _jacobianOplusXi(2,2) = _jacobianOplusXi(0,2);
  _jacobianOplusXi(2,3) = _jacobianOplusXi(0,3);
  _jacobianOplusXi(2,4) = 0;
  _jacobianOplusXi(2,5) = _jacobianOplusXi(0,5)-bf*invz_2;
}

EdgeR3ConstraintPoseVel::~EdgeR3ConstraintPoseVel(){}
bool EdgeR3ConstraintPoseVel::read(std::istream& is){
  for (int i=0; i<9; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<9; i++)
    for (int j=i; j<9; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeR3ConstraintPoseVel::write(std::ostream& os) const {

  for (int i=0; i<9; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<9; i++)
    for (int j=i; j<9; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}
Vector3d EdgeR3ConstraintPoseVel::RotErrCompute(const Quaterniond & _rwbj) const {

        Vector3d omega;
        omega = JRg*Bgi;
        double theta = omega.norm();
        Matrix3d Omega = skew(omega);
        Matrix3d R;
        if (theta<0.00001)
        {
          //TODO: CHECK WHETHER THIS IS CORRECT!!!
          R = (Matrix3d::Identity() + Omega + Omega*Omega);
        }
        else
        {
          Matrix3d Omega2 = Omega*Omega;

          R = (Matrix3d::Identity()
              + sin(theta)/theta *Omega
              + (1-cos(theta))/(theta*theta)*Omega2);
        }
        Vector3d res;
        Matrix3d _R = DeltaR*R.transpose()*Rwbi.inverse()*_rwbj.toRotationMatrix();
        double d =  0.5*(_R(0,0)+_R(1,1)+_R(2,2)-1);
       // Vector3d omega;
        Vector3d dR = deltaR(_R);
        
        if (d>0.99999)
        {
          omega=0.5*dR;
          //Matrix3d Omega = skew(omega);
        }
        else
        {
          double theta = acos(d);
          omega = theta/(2*sqrt(1-d*d))*dR;
          //Matrix3d Omega = skew(omega);
        }

        for (int i=0; i<3;i++){
          res[i]=omega[i];
        }
        return res;

}

Vector3d EdgeR3ConstraintPoseVel::VelErrCompute(const Vector3d & vwbj) const {

  Vector3d res;
  res = Rwbi.transpose()*(vwbj - Vwbi - (Gw*timeDiffKFs)) - (DeltaV + (JVg*Bgi) + (JVa*Bai));
  return res;
}
Vector3d EdgeR3ConstraintPoseVel::PosErrCompute(const Vector3d & pwbj) const {

  Vector3d res;
  res = Rwbi.transpose()*(pwbj - Pwbi - (Vwbi*timeDiffKFs) - (0.5*Gw*timeDiffKFs*timeDiffKFs)) - (DeltaP + (JPg*Bgi) + (JPa*Bai));
  return res;
}
Vector9d EdgeR3ConstraintPoseVel::concatenateVecs(const Vector3d & Rot,const Vector3d & Vel, const Vector3d & Pose) const{

  Vector9d res;
  res[0] = Rot(0);
  res[1] = Rot(1);
  res[2] = Rot(2);
  res[3] = Vel(0);
  res[4] = Vel(1);
  res[5] = Vel(2);
  res[6] = Pose(0);
  res[7] = Pose(1);
  res[8] = Pose(2);
  return res;

}
 void EdgeR3ConstraintPoseVel::linearizeOplus()
 {
  Vertex15SE3Expmap* v1 = static_cast<Vertex15SE3Expmap*>(_vertices[0]);
  Quaterniond Rwbj = v1->estimate().rotation();
  //Vector3d Vwbj = v1->estimate().velocity();
  //Vector3d Pwbj = v1->estimate().translation();
  Matrix3d I = Matrix3d::Identity();
  //double Ri = deltaR(Rwbi).norm();
  Matrix3d Rj = Rwbj.toRotationMatrix();
  MatrixPower<Matrix3d> PowRj(Rj);
  double Rjn = deltaR(Rj).norm();
  Eigen::Matrix<double,3,3> zeros;
  zeros << 0,0,0,
           0,0,0,
           0,0,0;
  _jacobianOplusXi.block<3,3>(0,0) = -(I + 0.5*Rj + ((1/(pow(Rjn,2))) + ((1 + cos(Rjn))/(2*Rjn*sin(Rjn))))*PowRj(2));
  _jacobianOplusXi.block<3,3>(3,0) = zeros;
  _jacobianOplusXi.block<3,3>(6,0) = zeros;
  _jacobianOplusXi.block<3,3>(0,3) = zeros;
  _jacobianOplusXi.block<3,3>(3,3) = Rwbi.transpose();
  _jacobianOplusXi.block<3,3>(6,3) = zeros;
  _jacobianOplusXi.block<3,3>(0,6) = zeros;
  _jacobianOplusXi.block<3,3>(3,6) = Rwbi.transpose()*Rj;
  _jacobianOplusXi.block<3,3>(6,6) = zeros;
}

EdgeR3ConstraintPoseVelLinked::~EdgeR3ConstraintPoseVelLinked(){}
bool EdgeR3ConstraintPoseVelLinked::read(std::istream& is){
  for (int i=0; i<9; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<9; i++)
    for (int j=i; j<9; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeR3ConstraintPoseVelLinked::write(std::ostream& os) const {

  for (int i=0; i<9; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<9; i++)
    for (int j=i; j<9; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}
Vector3d EdgeR3ConstraintPoseVelLinked::RotErrCompute(const Quaterniond & _rwbj) const {

        Vector3d omega;
        omega = JRg*Bgi;
        double theta = omega.norm();
        Matrix3d Omega = skew(omega);

        Matrix3d R;
        if (theta<0.00001)
        {
          //TODO: CHECK WHETHER THIS IS CORRECT!!!
          R = (Matrix3d::Identity() + Omega + Omega*Omega);
        }
        else
        {
          Matrix3d Omega2 = Omega*Omega;

          R = (Matrix3d::Identity()
              + sin(theta)/theta *Omega
              + (1-cos(theta))/(theta*theta)*Omega2);
        }
        Vector3d res;
        Matrix3d _R = DeltaR*R.transpose()*Rwbi.inverse()*_rwbj.toRotationMatrix();
        double d =  0.5*(_R(0,0)+_R(1,1)+_R(2,2)-1);
       // Vector3d omega;
        Vector3d dR = deltaR(_R);
        
        if (d>0.99999)
        {
          omega=0.5*dR;
          //Matrix3d Omega = skew(omega);
        }
        else
        {
          double theta = acos(d);
          omega = theta/(2*sqrt(1-d*d))*dR;
         // Matrix3d Omega = skew(omega);
        }

        for (int i=0; i<3;i++){
          res[i]=omega[i];
        }
        return res;

}

Vector3d EdgeR3ConstraintPoseVelLinked::VelErrCompute(const Vector3d & vwbj) const {

  Vector3d res;
  res = Rwbi.transpose()*(vwbj - Vwbi - (Gw*timeDiffKFs)) - (DeltaV + (JVg*Bgi) + (JVa*Bai));
  return res;
}
Vector3d EdgeR3ConstraintPoseVelLinked::PosErrCompute(const Vector3d & pwbj) const {

  Vector3d res;
  res = Rwbi.transpose()*(pwbj - Pwbi - (Vwbi*timeDiffKFs) - (0.5*Gw*timeDiffKFs*timeDiffKFs)) - (DeltaP + (JPg*Bgi) + (JPa*Bai));
  return res;
}
Vector9d EdgeR3ConstraintPoseVelLinked::concatenateVecs(const Vector3d & Rot,const Vector3d & Vel, const Vector3d & Pose) const{

  Vector9d res;
  res[0] = Rot(0);
  res[1] = Rot(1);
  res[2] = Rot(2);
  res[3] = Vel(0);
  res[4] = Vel(1);
  res[5] = Vel(2);
  res[6] = Pose(0);
  res[7] = Pose(1);
  res[8] = Pose(2);
  return res;

}
 void EdgeR3ConstraintPoseVelLinked::linearizeOplus()
 {
  Vertex15SE3Expmap* v1 = static_cast<Vertex15SE3Expmap*>(_vertices[1]);
  Quaterniond Rwbj = v1->estimate().rotation();
  Vector3d vwbj = v1->estimate().velocity();
  Vector3d pwbj = v1->estimate().translation();
  Matrix3d I = Matrix3d::Identity();
  double Ri = deltaR(Rwbi).norm();
  Matrix3d Rj = Rwbj.toRotationMatrix();
  double Rjn = deltaR(Rj).norm();
  MatrixPower<Matrix3d> PowRj(Rj);
  MatrixPower<Matrix3d> PowRwbi(Rwbi);
  Eigen::Matrix<double,3,3> zeros;
  zeros << 0,0,0,
           0,0,0,
           0,0,0;

  _jacobianOplusXi.block<3,3>(0,0) = -(I + 0.5*Rwbi + (1/(pow(Ri,2)) + ((1 + cos(Ri))/(2*Ri*sin(Ri))))*PowRwbi(2))*Rj.transpose()*Rwbi;
  _jacobianOplusXi.block<3,3>(3,0) = zeros;
  _jacobianOplusXi.block<3,3>(6,0) = zeros;
  _jacobianOplusXi.block<3,3>(0,3) = skew(Rwbi.transpose()*(vwbj - Vwbi - (Gw*timeDiffKFs)));
  _jacobianOplusXi.block<3,3>(3,3) = -Rwbi.transpose();
  _jacobianOplusXi.block<3,3>(6,3) = zeros;
  _jacobianOplusXi.block<3,3>(0,6) = skew(Rwbi.transpose()*(pwbj - Pwbi - (Vwbi*timeDiffKFs) - (0.5*Gw*timeDiffKFs*timeDiffKFs)));
  _jacobianOplusXi.block<3,3>(3,6) = -Rwbi.transpose()*timeDiffKFs;
  _jacobianOplusXi.block<3,3>(6,6) = -I;

  _jacobianOplusXj.block<3,3>(0,0) = -(I + 0.5*Rj + (1/(pow(Rjn,2)) + ((1 + cos(Rjn))/(2*Rjn*sin(Rjn))))*PowRj(2));
  _jacobianOplusXj.block<3,3>(3,0) = zeros;
  _jacobianOplusXj.block<3,3>(6,0) = zeros;
  _jacobianOplusXj.block<3,3>(0,3) = zeros;
  _jacobianOplusXj.block<3,3>(3,3) = Rwbi.transpose();
  _jacobianOplusXj.block<3,3>(6,3) = zeros;
  _jacobianOplusXj.block<3,3>(0,6) = zeros;
  _jacobianOplusXj.block<3,3>(3,6) = Rwbi.transpose()*Rj;
  _jacobianOplusXj.block<3,3>(6,6) = zeros;
}
EdgeR3ConstraintBiasLinked::~EdgeR3ConstraintBiasLinked(){}
bool EdgeR3ConstraintBiasLinked::read(std::istream& is){
  for (int i=0; i<6; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<6; i++)
    for (int j=i; j<6; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeR3ConstraintBiasLinked::write(std::ostream& os) const {

  for (int i=0; i<6; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<6; i++)
    for (int j=i; j<6; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}
Vector6d EdgeR3ConstraintBiasLinked::concatenateVecs(const Vector3d & a,const Vector3d & g){
  Vector6d res;
  res[0] = g(0);
  res[1] = g(1);
  res[2] = g(2);
  res[3] = a(0);
  res[4] = a(1);
  res[5] = a(2);
  return res;
}
void EdgeR3ConstraintBiasLinked::linearizeOplus()
{

  Vertex15SE3Expmap* v1 = static_cast<Vertex15SE3Expmap*>(_vertices[0]);
  Vertex15SE3Expmap* v2 = static_cast<Vertex15SE3Expmap*>(_vertices[1]);

  Quaterniond _rwbj = v2->estimate().rotation();
  Quaterniond Rwbi = v1->estimate().rotation();

  Vector3d deltaBg = v2->estimate().gyroBias() - v1->estimate().gyroBias();
  Vector3d omega;
  omega = JRg*deltaBg;
  double theta = omega.norm();
  Matrix3d Omega = skew(omega);

  Matrix3d R;
  if (theta<0.00001)
  {
    //TODO: CHECK WHETHER THIS IS CORRECT!!!
    R = (Matrix3d::Identity() + Omega + Omega*Omega);
  }
  else
  {
    Matrix3d Omega2 = Omega*Omega;

    R = (Matrix3d::Identity()
        + sin(theta)/theta *Omega
        + (1-cos(theta))/(theta*theta)*Omega2);
  }
  Vector3d res;
  Matrix3d _R = DeltaR*R.transpose()*Rwbi.toRotationMatrix().inverse()*_rwbj.toRotationMatrix();
  double d =  0.5*(_R(0,0)+_R(1,1)+_R(2,2)-1);
 // Vector3d omega;
  Vector3d dR = deltaR(_R);
  
  if (d>0.99999)
  {
    omega=0.5*dR;
    //Matrix3d Omega = skew(omega);
  }
  else
  {
    double theta = acos(d);
    omega = theta/(2*sqrt(1-d*d))*dR;
   // Matrix3d Omega = skew(omega);
  }

  for (int i=0; i<3;i++){
    res[i]=omega[i];
  }

  Matrix3d I = Matrix3d::Identity();
  double rri = res.norm();
  Matrix3d rRi = skew(res);
  MatrixPower<Matrix3d> PowRri(rRi);

  Matrix3d Jr_inv = -(I + 0.5*rRi + (1/(pow(rri,2)) + ((1 + cos(rri))/(2*rri*sin(rri))))*PowRri(2));

  Vector3d res2 = JRg*deltaBg;
  double oi = res2.norm();
  Matrix3d oRi = skew(res2);
  MatrixPower<Matrix3d> PowOri(rRi);

  Matrix3d JrR = I - ((1 - cos(oi))/pow(oi,2))*oRi + ((oi - sin(oi))/pow(oi,3))*PowOri(2);


  theta = res.norm();
  Omega = skew(res);

  Matrix3d ExpR;
  if (theta<0.00001)
  {
    //TODO: CHECK WHETHER THIS IS CORRECT!!!
    ExpR = (Matrix3d::Identity() + Omega + Omega*Omega);
  }
  else
  {
    Matrix3d Omega2 = Omega*Omega;

    ExpR = (Matrix3d::Identity()
        + sin(theta)/theta *Omega
        + (1-cos(theta))/(theta*theta)*Omega2);
  }

  alpha = Jr_inv*ExpR.transpose()*JrR*JRg;

  Eigen::Matrix<double,3,3> zeros;
  zeros << 0,0,0,
           0,0,0,
           0,0,0;
  _jacobianOplusXi.block<3,3>(0,0) =alpha;
  _jacobianOplusXi.block<3,3>(3,0) = -JVg;
  _jacobianOplusXi.block<3,3>(6,0) = -JPg;
  _jacobianOplusXi.block<3,3>(0,3) = zeros;
  _jacobianOplusXi.block<3,3>(3,3) = -JVa;
  _jacobianOplusXi.block<3,3>(6,3) = -JPa;

  _jacobianOplusXj.block<3,3>(0,0) = alpha;
  _jacobianOplusXj.block<3,3>(3,0) = -JVg;
  _jacobianOplusXj.block<3,3>(6,0) = -JPg;
  _jacobianOplusXj.block<3,3>(0,3) = zeros;
  _jacobianOplusXj.block<3,3>(3,3) = -JVa;
  _jacobianOplusXj.block<3,3>(6,3) = -JPa;
}
EdgeR3ConstraintBias::~EdgeR3ConstraintBias(){}
bool EdgeR3ConstraintBias::read(std::istream& is){
  for (int i=0; i<6; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<6; i++)
    for (int j=i; j<6; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeR3ConstraintBias::write(std::ostream& os) const {

  for (int i=0; i<6; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<6; i++)
    for (int j=i; j<6; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}
Vector6d EdgeR3ConstraintBias::concatenateVecs(const Vector3d & a,const Vector3d & g){
  Vector6d res;
  res[0] = g(0);
  res[1] = g(1);
  res[2] = g(2);
  res[3] = a(0);
  res[4] = a(1);
  res[5] = a(2);

  return res;

}
void EdgeR3ConstraintBias::linearizeOplus()
{

  //Vertex15SE3Expmap* v1 = static_cast<Vertex15SE3Expmap*>(_vertices[0]);
  Vertex15SE3Expmap* v2 = static_cast<Vertex15SE3Expmap*>(_vertices[1]);

  Quaterniond _rwbj = v2->estimate().rotation();
  //Matrix3d Rwbi = v1->estimate().rotation();
  Vector3d v1g;
  v1g << _measurement[0],
         _measurement[1],
         _measurement[2];

  Vector3d deltaBg = v2->estimate().gyroBias() - v1g;
  Vector3d omega;
  omega = JRg*deltaBg;
  double theta = omega.norm();
  Matrix3d Omega = skew(omega);

  Matrix3d R;
  if (theta<0.00001)
  {
    //TODO: CHECK WHETHER THIS IS CORRECT!!!
    R = (Matrix3d::Identity() + Omega + Omega*Omega);
  }
  else
  {
    Matrix3d Omega2 = Omega*Omega;

    R = (Matrix3d::Identity()
        + sin(theta)/theta *Omega
        + (1-cos(theta))/(theta*theta)*Omega2);
  }
  Vector3d res;
  Matrix3d _R = DeltaR*R.transpose()*Rwbi.inverse()*_rwbj.toRotationMatrix();
  double d =  0.5*(_R(0,0)+_R(1,1)+_R(2,2)-1);
 // Vector3d omega;
  Vector3d dR = deltaR(_R);
  
  if (d>0.99999)
  {
    omega=0.5*dR;
    //Matrix3d Omega = skew(omega);
  }
  else
  {
    double theta = acos(d);
    omega = theta/(2*sqrt(1-d*d))*dR;
   // Matrix3d Omega = skew(omega);
  }

  for (int i=0; i<3;i++){
    res[i]=omega[i];
  }

  Matrix3d I = Matrix3d::Identity();
  double rri = res.norm();
  Matrix3d rRi = skew(res);
  MatrixPower<Matrix3d> PowRri(rRi);

  Matrix3d Jr_inv = -(I + 0.5*rRi + (1/(pow(rri,2)) + ((1 + cos(rri))/(2*rri*sin(rri))))*PowRri(2));

  Vector3d res2 = JRg*deltaBg;
  double oi = res2.norm();
  Matrix3d oRi = skew(res2);
  MatrixPower<Matrix3d> PowOri(rRi);

  Matrix3d JrR = I - ((1 - cos(oi))/pow(oi,2))*oRi + ((oi - sin(oi))/pow(oi,3))*PowOri(2);


  theta = res.norm();
  Omega = skew(res);

  Matrix3d ExpR;
  if (theta<0.00001)
  {
    //TODO: CHECK WHETHER THIS IS CORRECT!!!
    ExpR = (Matrix3d::Identity() + Omega + Omega*Omega);
  }
  else
  {
    Matrix3d Omega2 = Omega*Omega;

    ExpR = (Matrix3d::Identity()
        + sin(theta)/theta *Omega
        + (1-cos(theta))/(theta*theta)*Omega2);
  }

  alpha = Jr_inv*ExpR.transpose()*JrR*JRg;

  Eigen::Matrix<double,3,3> zeros;
  zeros << 0,0,0,
           0,0,0,
           0,0,0;
  _jacobianOplusXi.block<3,3>(0,0) =alpha;
  _jacobianOplusXi.block<3,3>(3,0) = -JVg;
  _jacobianOplusXi.block<3,3>(6,0) = -JPg;
  _jacobianOplusXi.block<3,3>(0,3) = zeros;
  _jacobianOplusXi.block<3,3>(3,3) = -JVa;
  _jacobianOplusXi.block<3,3>(6,3) = -JPa;
}

EdgeSE3ProjectXYZIMU::EdgeSE3ProjectXYZIMU() : BaseBinaryEdge<2, Vector2d, VertexSBAPointXYZ, Vertex15SE3Expmap>() {
}

bool EdgeSE3ProjectXYZIMU::read(std::istream& is){
  for (int i=0; i<2; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeSE3ProjectXYZIMU::write(std::ostream& os) const {

  for (int i=0; i<2; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}


void EdgeSE3ProjectXYZIMU::linearizeOplus() {
  VertexSE3Expmap * vj = static_cast<VertexSE3Expmap *>(_vertices[1]);
  SE3Quat T(vj->estimate());
  VertexSBAPointXYZ* vi = static_cast<VertexSBAPointXYZ*>(_vertices[0]);
  Vector3d xyz = vi->estimate();
  Vector3d xyz_trans = T.map(xyz);

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double z = xyz_trans[2];
  double z_2 = z*z;

  Matrix<double,2,3> tmp;
  tmp(0,0) = fx;
  tmp(0,1) = 0;
  tmp(0,2) = -x/z*fx;

  tmp(1,0) = 0;
  tmp(1,1) = fy;
  tmp(1,2) = -y/z*fy;

  _jacobianOplusXi =  -1./z * tmp * T.rotation().toRotationMatrix();

  _jacobianOplusXj(0,0) =  x*y/z_2 *fx;
  _jacobianOplusXj(0,1) = -(1+(x*x/z_2)) *fx;
  _jacobianOplusXj(0,2) = y/z *fx;
  _jacobianOplusXj(0,3) = -1./z *fx;
  _jacobianOplusXj(0,4) = 0;
  _jacobianOplusXj(0,5) = x/z_2 *fx;

  _jacobianOplusXj(1,0) = (1+y*y/z_2) *fy;
  _jacobianOplusXj(1,1) = -x*y/z_2 *fy;
  _jacobianOplusXj(1,2) = -x/z *fy;
  _jacobianOplusXj(1,3) = 0;
  _jacobianOplusXj(1,4) = -1./z *fy;
  _jacobianOplusXj(1,5) = y/z_2 *fy;
}

Vector2d EdgeSE3ProjectXYZIMU::cam_project(const Vector3d & trans_xyz) const{
  Vector2d proj = project2d(trans_xyz);
  Vector2d res;
  res[0] = proj[0]*fx + cx;
  res[1] = proj[1]*fy + cy;
  return res;
}

Vector3d EdgeStereoSE3ProjectXYZIMU::cam_project(const Vector3d & trans_xyz, const float &bf) const{
  const float invz = 1.0f/trans_xyz[2];
  Vector3d res;
  res[0] = trans_xyz[0]*invz*fx + cx;
  res[1] = trans_xyz[1]*invz*fy + cy;
  res[2] = res[0] - bf*invz;
  return res;
}

EdgeStereoSE3ProjectXYZIMU::EdgeStereoSE3ProjectXYZIMU() : BaseBinaryEdge<3, Vector3d, VertexSBAPointXYZ, Vertex15SE3Expmap>() {
}

bool EdgeStereoSE3ProjectXYZIMU::read(std::istream& is){
  for (int i=0; i<=3; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeStereoSE3ProjectXYZIMU::write(std::ostream& os) const {

  for (int i=0; i<=3; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}

void EdgeStereoSE3ProjectXYZIMU::linearizeOplus() {
  VertexSE3Expmap * vj = static_cast<VertexSE3Expmap *>(_vertices[1]);
  SE3Quat T(vj->estimate());
  VertexSBAPointXYZ* vi = static_cast<VertexSBAPointXYZ*>(_vertices[0]);
  Vector3d xyz = vi->estimate();
  Vector3d xyz_trans = T.map(xyz);

  const Matrix3d R =  T.rotation().toRotationMatrix();

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double z = xyz_trans[2];
  double z_2 = z*z;

  _jacobianOplusXi(0,0) = -fx*R(0,0)/z+fx*x*R(2,0)/z_2;
  _jacobianOplusXi(0,1) = -fx*R(0,1)/z+fx*x*R(2,1)/z_2;
  _jacobianOplusXi(0,2) = -fx*R(0,2)/z+fx*x*R(2,2)/z_2;

  _jacobianOplusXi(1,0) = -fy*R(1,0)/z+fy*y*R(2,0)/z_2;
  _jacobianOplusXi(1,1) = -fy*R(1,1)/z+fy*y*R(2,1)/z_2;
  _jacobianOplusXi(1,2) = -fy*R(1,2)/z+fy*y*R(2,2)/z_2;

  _jacobianOplusXi(2,0) = _jacobianOplusXi(0,0)-bf*R(2,0)/z_2;
  _jacobianOplusXi(2,1) = _jacobianOplusXi(0,1)-bf*R(2,1)/z_2;
  _jacobianOplusXi(2,2) = _jacobianOplusXi(0,2)-bf*R(2,2)/z_2;

  _jacobianOplusXj(0,0) =  x*y/z_2 *fx;
  _jacobianOplusXj(0,1) = -(1+(x*x/z_2)) *fx;
  _jacobianOplusXj(0,2) = y/z *fx;
  _jacobianOplusXj(0,3) = -1./z *fx;
  _jacobianOplusXj(0,4) = 0;
  _jacobianOplusXj(0,5) = x/z_2 *fx;

  _jacobianOplusXj(1,0) = (1+y*y/z_2) *fy;
  _jacobianOplusXj(1,1) = -x*y/z_2 *fy;
  _jacobianOplusXj(1,2) = -x/z *fy;
  _jacobianOplusXj(1,3) = 0;
  _jacobianOplusXj(1,4) = -1./z *fy;
  _jacobianOplusXj(1,5) = y/z_2 *fy;

  _jacobianOplusXj(2,0) = _jacobianOplusXj(0,0)-bf*y/z_2;
  _jacobianOplusXj(2,1) = _jacobianOplusXj(0,1)+bf*x/z_2;
  _jacobianOplusXj(2,2) = _jacobianOplusXj(0,2);
  _jacobianOplusXj(2,3) = _jacobianOplusXj(0,3);
  _jacobianOplusXj(2,4) = 0;
  _jacobianOplusXj(2,5) = _jacobianOplusXj(0,5)-bf/z_2;
}


//Only Pose

bool EdgeSE3ProjectXYZOnlyPoseIMU::read(std::istream& is){
  for (int i=0; i<2; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeSE3ProjectXYZOnlyPoseIMU::write(std::ostream& os) const {

  for (int i=0; i<2; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<2; i++)
    for (int j=i; j<2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}


void EdgeSE3ProjectXYZOnlyPoseIMU::linearizeOplus() {
  VertexSE3Expmap * vi = static_cast<VertexSE3Expmap *>(_vertices[0]);
  Vector3d xyz_trans = vi->estimate().map(Xw);

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double invz = 1.0/xyz_trans[2];
  double invz_2 = invz*invz;

  _jacobianOplusXi(0,0) =  x*y*invz_2 *fx;
  _jacobianOplusXi(0,1) = -(1+(x*x*invz_2)) *fx;
  _jacobianOplusXi(0,2) = y*invz *fx;
  _jacobianOplusXi(0,3) = -invz *fx;
  _jacobianOplusXi(0,4) = 0;
  _jacobianOplusXi(0,5) = x*invz_2 *fx;

  _jacobianOplusXi(1,0) = (1+y*y*invz_2) *fy;
  _jacobianOplusXi(1,1) = -x*y*invz_2 *fy;
  _jacobianOplusXi(1,2) = -x*invz *fy;
  _jacobianOplusXi(1,3) = 0;
  _jacobianOplusXi(1,4) = -invz *fy;
  _jacobianOplusXi(1,5) = y*invz_2 *fy;
}

Vector2d EdgeSE3ProjectXYZOnlyPoseIMU::cam_project(const Vector3d & trans_xyz) const{
  Vector2d proj = project2d(trans_xyz);
  Vector2d res;
  res[0] = proj[0]*fx + cx;
  res[1] = proj[1]*fy + cy;
  return res;
}


Vector3d EdgeStereoSE3ProjectXYZOnlyPoseIMU::cam_project(const Vector3d & trans_xyz) const{
  const float invz = 1.0f/trans_xyz[2];
  Vector3d res;
  res[0] = trans_xyz[0]*invz*fx + cx;
  res[1] = trans_xyz[1]*invz*fy + cy;
  res[2] = res[0] - bf*invz;
  return res;
}


bool EdgeStereoSE3ProjectXYZOnlyPoseIMU::read(std::istream& is){
  for (int i=0; i<=3; i++){
    is >> _measurement[i];
  }
  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++) {
      is >> information()(i,j);
      if (i!=j)
        information()(j,i)=information()(i,j);
    }
  return true;
}

bool EdgeStereoSE3ProjectXYZOnlyPoseIMU::write(std::ostream& os) const {

  for (int i=0; i<=3; i++){
    os << measurement()[i] << " ";
  }

  for (int i=0; i<=2; i++)
    for (int j=i; j<=2; j++){
      os << " " <<  information()(i,j);
    }
  return os.good();
}

void EdgeStereoSE3ProjectXYZOnlyPoseIMU::linearizeOplus() {
  VertexSE3Expmap * vi = static_cast<VertexSE3Expmap *>(_vertices[0]);
  Vector3d xyz_trans = vi->estimate().map(Xw);

  double x = xyz_trans[0];
  double y = xyz_trans[1];
  double invz = 1.0/xyz_trans[2];
  double invz_2 = invz*invz;

  _jacobianOplusXi(0,0) =  x*y*invz_2 *fx;
  _jacobianOplusXi(0,1) = -(1+(x*x*invz_2)) *fx;
  _jacobianOplusXi(0,2) = y*invz *fx;
  _jacobianOplusXi(0,3) = -invz *fx;
  _jacobianOplusXi(0,4) = 0;
  _jacobianOplusXi(0,5) = x*invz_2 *fx;

  _jacobianOplusXi(1,0) = (1+y*y*invz_2) *fy;
  _jacobianOplusXi(1,1) = -x*y*invz_2 *fy;
  _jacobianOplusXi(1,2) = -x*invz *fy;
  _jacobianOplusXi(1,3) = 0;
  _jacobianOplusXi(1,4) = -invz *fy;
  _jacobianOplusXi(1,5) = y*invz_2 *fy;

  _jacobianOplusXi(2,0) = _jacobianOplusXi(0,0)-bf*y*invz_2;
  _jacobianOplusXi(2,1) = _jacobianOplusXi(0,1)+bf*x*invz_2;
  _jacobianOplusXi(2,2) = _jacobianOplusXi(0,2);
  _jacobianOplusXi(2,3) = _jacobianOplusXi(0,3);
  _jacobianOplusXi(2,4) = 0;
  _jacobianOplusXi(2,5) = _jacobianOplusXi(0,5)-bf*invz_2;
}

} // end namespace
