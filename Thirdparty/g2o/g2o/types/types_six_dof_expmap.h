// g2o - General Graph Optimization
// Copyright (C) 2011 H. Strasdat
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Modified by Raúl Mur Artal (2014)
// Added EdgeSE3ProjectXYZ (project using focal_length in x,y directions)
// Modified by Raúl Mur Artal (2016)
// Added EdgeStereoSE3ProjectXYZ (project using focal_length in x,y directions)
// Added EdgeSE3ProjectXYZOnlyPose (unary edge to optimize only the camera pose)
// Added EdgeStereoSE3ProjectXYZOnlyPose (unary edge to optimize only the camera pose)

#ifndef G2O_SIX_DOF_TYPES_EXPMAP
#define G2O_SIX_DOF_TYPES_EXPMAP

#include "../core/base_vertex.h"
#include "../core/base_binary_edge.h"
#include "../core/base_unary_edge.h"
#include "se3_ops.h"
#include "se3quatr3vb.h"
#include "se3quat.h"
#include "types_sba.h"
#include <type_traits>
#include <Eigen/Geometry>

namespace g2o {
namespace types_six_dof_expmap {
void init();
}

using namespace Eigen;

typedef Matrix<double, 6, 6> Matrix6d;
typedef Matrix<double, 9, 1> Vector9d;
//typedef Matrix<const double, Dynamic,1> Vector15dd;


/**
 * \brief SE3 Vertex parameterized internally with a transformation matrix
 and externally with its exponential map
 */
class  VertexSE3Expmap : public BaseVertex<6, SE3Quat>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  VertexSE3Expmap();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  virtual void setToOriginImpl() {
    _estimate = SE3Quat();
  }

  virtual void oplusImpl(const double* update_)  {
    Eigen::Map<const Vector6d> update(update_);
    setEstimate(SE3Quat::exp(update)*estimate());
  }
};

class Vertex15SE3Expmap : public BaseVertex<15, SE3QuatR3VB>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  Vertex15SE3Expmap();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  virtual void setToOriginImpl() {
    _estimate = SE3QuatR3VB();
  }

  virtual void oplusImpl(const double* update_){
    Eigen::Map<const Vector15d> update(update_);
    setEstimate(SE3QuatR3VB::exp(update)*estimate());
    //const Eigen::MatrixXd update(15,1);
    //double up = remove;
    //std::remove_const<*update_>::type up;
    /*for(int i=0;i<15;i++)
    {
      update(i) = update_;
    }*/
    //const Vector15dd update;
    //update.col(0) << up,up,up,up,up,up,up,up,up,up,up,up,up,up,up;
    //setEstimate(SE3QuatR3VB::exp(update)*estimate());
  }
};

class  EdgeSE3ProjectXYZ: public  BaseBinaryEdge<2, Vector2d, VertexSBAPointXYZ, VertexSE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeSE3ProjectXYZ();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    Vector2d obs(_measurement);
    _error = obs-cam_project(v1->estimate().map(v2->estimate()));
  }

  bool isDepthPositive() {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    return (v1->estimate().map(v2->estimate()))(2)>0.0;
  }
    

  virtual void linearizeOplus();

  Vector2d cam_project(const Vector3d & trans_xyz) const;

  double fx, fy, cx, cy;
};

class  EdgeStereoSE3ProjectXYZ: public  BaseBinaryEdge<3, Vector3d, VertexSBAPointXYZ, VertexSE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeStereoSE3ProjectXYZ();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    Vector3d obs(_measurement);
    _error = obs - cam_project(v1->estimate().map(v2->estimate()),bf);
  }

  bool isDepthPositive() {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    return (v1->estimate().map(v2->estimate()))(2)>0.0;
  }


  virtual void linearizeOplus();

  Vector3d cam_project(const Vector3d & trans_xyz, const float &bf) const;

  double fx, fy, cx, cy, bf;
};

class  EdgeSE3ProjectXYZOnlyPose: public  BaseUnaryEdge<2, Vector2d, VertexSE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeSE3ProjectXYZOnlyPose(){}

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[0]);
    Vector2d obs(_measurement);
    _error = obs-cam_project(v1->estimate().map(Xw));
  }

  bool isDepthPositive() {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[0]);
    return (v1->estimate().map(Xw))(2)>0.0;
  }


  virtual void linearizeOplus();

  Vector2d cam_project(const Vector3d & trans_xyz) const;

  Vector3d Xw;
  double fx, fy, cx, cy;
};


class  EdgeStereoSE3ProjectXYZOnlyPose: public  BaseUnaryEdge<3, Vector3d, VertexSE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeStereoSE3ProjectXYZOnlyPose(){}

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[0]);
    Vector3d obs(_measurement);
    _error = obs - cam_project(v1->estimate().map(Xw));
  }

  bool isDepthPositive() {
    const VertexSE3Expmap* v1 = static_cast<const VertexSE3Expmap*>(_vertices[0]);
    return (v1->estimate().map(Xw))(2)>0.0;
  }


  virtual void linearizeOplus();

  Vector3d cam_project(const Vector3d & trans_xyz) const;

  Vector3d Xw;
  double fx, fy, cx, cy, bf;
};

class EdgeR3ConstraintPoseVel: public BaseUnaryEdge<9,Vector9d,Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeR3ConstraintPoseVel(){}
  ~EdgeR3ConstraintPoseVel();
  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    const Quaterniond Rwbj = v1->estimate().rotation();
    const Vector3d Vwbj = v1->estimate().velocity();
    const Vector3d Pwbj = v1->estimate().translation();

    Vector9d deltaState(concatenateVecs(RotErrCompute(Rwbj),PosErrCompute(Pwbj),VelErrCompute(Vwbj)));
    _error = deltaState;
  }

  Vector3d RotErrCompute(const Quaterniond & _rwbj) const;
  Vector3d VelErrCompute(const Vector3d & vwbj) const;
  Vector3d PosErrCompute(const Vector3d & pwbj) const;

  Vector9d concatenateVecs(const Vector3d & Rot,const Vector3d & Vel, const Vector3d & Pose) const;
  virtual void linearizeOplus();

  Matrix3d Rwbi;
  Vector3d Vwbi, Pwbi; 
  Matrix3d DeltaR;
  Vector3d DeltaV, DeltaP;
  Matrix3d JRg, JVg, JVa, JPg, JPa, Jr;
  Vector3d Bgi, Bai;
  double timeDiffKFs;
  Vector3d Gw;

};

class EdgeR3ConstraintPoseVelLinked: public BaseBinaryEdge<9,Vector9d,Vertex15SE3Expmap,Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeR3ConstraintPoseVelLinked(){}
  ~EdgeR3ConstraintPoseVelLinked();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    const Vertex15SE3Expmap* v2 = static_cast<const Vertex15SE3Expmap*>(_vertices[1]);
    Rwbi = v1->estimate().rotation().toRotationMatrix();
    Vwbi = v1->estimate().velocity();
    Pwbi = v1->estimate().translation();
    const Quaterniond Rwbj = v2->estimate().rotation();
    const Vector3d Vwbj = v2->estimate().velocity();
    const Vector3d Pwbj = v2->estimate().translation();
    Vector9d obs(concatenateVecs(RotErrCompute(Rwbj),PosErrCompute(Pwbj),VelErrCompute(Vwbj)));
    _error = obs;
  }

  Vector3d RotErrCompute(const Quaterniond & _rwbj) const;
  Vector3d VelErrCompute(const Vector3d & vwbj) const;
  Vector3d PosErrCompute(const Vector3d & pwbj) const;

  Vector9d concatenateVecs(const Vector3d & Rot,const Vector3d & Vel, const Vector3d & Pose) const;
  virtual void linearizeOplus();

  Matrix3d Rwbi;
  Vector3d Vwbi, Pwbi; 
  Matrix3d DeltaR;
  Vector3d DeltaV, DeltaP;
  Matrix3d JRg, JVg, JVa, JPg, JPa, Jr;
  Vector3d Bgi, Bai;
  double timeDiffKFs;
  Vector3d Gw;

};
//TODO: ALPHA VARIABLE IN MATRIX
class EdgeR3ConstraintBiasLinked: public BaseBinaryEdge<6,Vector6d,Vertex15SE3Expmap,Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeR3ConstraintBiasLinked(){}
  ~EdgeR3ConstraintBiasLinked();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    const Vertex15SE3Expmap* v2 = static_cast<const Vertex15SE3Expmap*>(_vertices[1]);
    Vector6d obs1(concatenateVecs(v1->estimate().gyroBias(),v1->estimate().accelBias()));
    Vector6d obs2(concatenateVecs(v2->estimate().gyroBias(),v2->estimate().accelBias()));

    _error = obs2 - obs1;
  }
  Vector6d concatenateVecs(const Vector3d & a,const Vector3d & g);
  Matrix3d DeltaR, JRg, JVg, JVa, JPg, JPa, alpha;
  
  virtual void linearizeOplus();


};

//TODO: ALPHA VARIABLE IN MATRIX
class EdgeR3ConstraintBias: public BaseUnaryEdge<6,Vector6d,Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeR3ConstraintBias(){}
  ~EdgeR3ConstraintBias();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    Vector6d obs2(concatenateVecs(v1->estimate().gyroBias(),v1->estimate().accelBias()));
    Vector6d obs1(_measurement);

    _error = obs2 - obs1;
  }
  Vector6d concatenateVecs(const Vector3d & a,const Vector3d & g);
  
  Matrix3d DeltaR,Rwbi,JRg, JVg, JVa, JPg, JPa,alpha;
  virtual void linearizeOplus();

};

class  EdgeSE3ProjectXYZIMU: public  BaseBinaryEdge<2, Vector2d, VertexSBAPointXYZ, Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeSE3ProjectXYZIMU();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    Vector2d obs(_measurement);
    _error = obs-cam_project(v1->estimate().map(v2->estimate()));
  }

  bool isDepthPositive() {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    return (v1->estimate().map(v2->estimate()))(2)>0.0;
  }
    

  virtual void linearizeOplus();

  Vector2d cam_project(const Vector3d & trans_xyz) const;

  double fx, fy, cx, cy;
};
class  EdgeStereoSE3ProjectXYZIMU: public  BaseBinaryEdge<3, Vector3d, VertexSBAPointXYZ, Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeStereoSE3ProjectXYZIMU();

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    Vector3d obs(_measurement);
    _error = obs - cam_project(v1->estimate().map(v2->estimate()),bf);
  }

  bool isDepthPositive() {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[1]);
    const VertexSBAPointXYZ* v2 = static_cast<const VertexSBAPointXYZ*>(_vertices[0]);
    return (v1->estimate().map(v2->estimate()))(2)>0.0;
  }


  virtual void linearizeOplus();

  Vector3d cam_project(const Vector3d & trans_xyz, const float &bf) const;

  double fx, fy, cx, cy, bf;
};

class  EdgeSE3ProjectXYZOnlyPoseIMU: public  BaseUnaryEdge<2, Vector2d, Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeSE3ProjectXYZOnlyPoseIMU(){}

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    Vector2d obs(_measurement);
    _error = obs-cam_project(v1->estimate().map(Xw));
  }

  bool isDepthPositive() {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    return (v1->estimate().map(Xw))(2)>0.0;
  }


  virtual void linearizeOplus();

  Vector2d cam_project(const Vector3d & trans_xyz) const;

  Vector3d Xw;
  double fx, fy, cx, cy;
};


class  EdgeStereoSE3ProjectXYZOnlyPoseIMU: public  BaseUnaryEdge<3, Vector3d, Vertex15SE3Expmap>{
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  EdgeStereoSE3ProjectXYZOnlyPoseIMU(){}

  bool read(std::istream& is);

  bool write(std::ostream& os) const;

  void computeError()  {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    Vector3d obs(_measurement);
    _error = obs - cam_project(v1->estimate().map(Xw));
  }

  bool isDepthPositive() {
    const Vertex15SE3Expmap* v1 = static_cast<const Vertex15SE3Expmap*>(_vertices[0]);
    return (v1->estimate().map(Xw))(2)>0.0;
  }


  virtual void linearizeOplus();

  Vector3d cam_project(const Vector3d & trans_xyz) const;

  Vector3d Xw;
  double fx, fy, cx, cy, bf;
};

} // end namespace

#endif
