#include "IMUFrame.h"
#include "Converter.h"

#include <thread>
#include <math.h>

namespace ORB_SLAM2
{
long unsigned int IMUFrame::nNextId=0;
IMUFrame::IMUFrame()
{}

IMUFrame::IMUFrame(const IMUFrame &Imuframe)
	: mJRg(Imuframe.mJRg.clone()), mJVg(Imuframe.mJVg.clone()), mJVa(Imuframe.mJVa.clone()), mJPg(Imuframe.mJPg.clone()), mJPa(Imuframe.mJPa.clone()),
	 mDeltaR(Imuframe.mDeltaR.clone()), mDeltaV(Imuframe.mDeltaV.clone()), mDeltaP(Imuframe.mDeltaP.clone()), mTimeStamp(Imuframe.mTimeStamp),
	  mBg(Imuframe.mBg.clone()), mBa(Imuframe.mBa.clone()), mnID(Imuframe.mnID)
{
	if(!Imuframe.mTcw.empty())
	{
		SetCamPose(Imuframe.mTcw);
	}
	if(!Imuframe.mRwb.empty())
	{
		SetState(Imuframe.mRwb, Imuframe.mVwb, Imuframe.mPwb);
	}
	if(!Imuframe.mTwb.empty())
	{
		Imuframe.mTwb.copyTo(mTwb);
	}
	if(!Imuframe.mEij.empty())
	{
		Imuframe.mEij.copyTo(mEij);
	}
}

IMUFrame::IMUFrame(KeyFrame* LastKF,const double &ax, const double &ay, const double &az, const double &gx, const double &gy, const double &gz,
 const double &timestamp, const bool &IntRdy, const cv::Mat &JRg, const cv::Mat &JVg, const cv::Mat &JVa
 , const cv::Mat &JPg, const cv::Mat &JPa, const cv::Mat &DeltaR, const cv::Mat &DeltaV, const cv::Mat &DeltaP, const cv::Mat &Eij)
: mTimeStamp(timestamp), mIntRdy(IntRdy)/*, mnewKeyFrame(newKeyFrame)*/, mJRg(JRg.clone()), mJVg(JVg.clone()), mJVa(JVa.clone()), mJPg(JPg.clone()),
 mJPa(JPa.clone()),mDeltaR(DeltaR.clone()), mDeltaV(DeltaV.clone()), mDeltaP(DeltaP.clone()), mEij(Eij.clone()) 
{
	// Frame ID
    mnID=nNextId++;
    

    NoiseCovIMU = cv::Mat::eye(6,6,CV_64F)*0.005;
    //cout << "im here" << endl;
    cv::Mat Gw1(3,1,CV_64F);
	Gw1.at<double>(0,0) = 0.0; Gw1.at<double>(1,0) = 0.0; Gw1.at<double>(2,0) = -9.81;
	Gw1.copyTo(Gw);

    setReferenceKF(LastKF);
    mBa = cv::Mat::zeros(3,1,CV_64F);
    mBg = cv::Mat::zeros(3,1,CV_64F);
    mRwb = cv::Mat::eye(3,3,CV_64F);
    mVwb = cv::Mat::zeros(3,1,CV_64F);
    mPwb = cv::Mat::zeros(3,1,CV_64F);  
    
    //cout << "start" << endl;
    UpdateJandPI(ax,ay,az,gx,gy,gz);

   // cout << "end" << endl;
    if(IntRdy)
    {
    	//cout << "start" << endl;
    	//mBg = LastKF->GetGyroBias();
		//mBa = LastKF->GetAccelBias();
		//mRwb = LastKF->GetIMURotation();
		//mVwb = LastKF->GetIMUVelocity();
		//mPwb = LastKF->GetIMUPosition();
    	IntegrateFrame();
    	computeInformationMatrix();
    	//cout << "end" << endl;

    }
    else
    {

    	UpdateCovariance(ax,ay,az,gx,gy,gz);
    	//cout << "im here" << endl;

    }
    

}
void IMUFrame::setReferenceKF(KeyFrame *mKF)
{
	/*
	if(!mKF->GetIMURotation().empty())
	{

	}
		mLKF = mKF;
	mBg = mLKF->Bg_i;
    mBa = mLKF->Ba_i; 
    mRwb = mLKF->Rwb_i
    mVwb = mLKF->Vwb_i; 
    mPwb = mLKF->Pwb_i 
    */
    
}
void IMUFrame::SetCamPose(cv::Mat Tcw)
{
	 mTcw = Tcw.clone();
	 mRcw = mTcw.rowRange(0,3).colRange(0,3);
     mRwc = mRcw.t();
     mtcw = mTcw.rowRange(0,3).col(3);
     mOw = -mRcw.t()*mtcw;
    

}
void IMUFrame::SetState(cv::Mat Rwb, cv::Mat Vwb, cv::Mat Pwb)
{
	Rwb.copyTo(mRwb);
	Vwb.copyTo(mVwb);
	Pwb.copyTo(mPwb);
}
void IMUFrame::UpdateJandPI(const double &ax, const double &ay, const double &az,const double &gx, const double &gy, const double &gz)
{
	cv::Mat gyro(3,1,CV_64F); cv::Mat accel(3,1,CV_64F); cv::Mat Rik(3,3,CV_64F);
	cv::Mat gyroP(3,1,CV_64F); cv::Mat accelP(3,1,CV_64F);

	gyro.at<double>(0,0) = gx; gyro.at<double>(1,0) = gy; gyro.at<double>(2,0) = gz;
	accel.at<double>(0,0) = ax; accel.at<double>(1,0) = ay; accel.at<double>(2,0) = az;

	// Subtract bias
	gyroP = gyro - mBg;
	accelP = accel - mBa;
	//bcout << gyro << endl;

	// Convert to skew symmetric matrix in S03
	Rik = Exp(gyroP*0.005);
    RikG = Rik.clone();
    // Preintegrations
	mDeltaR = mDeltaR*Rik;
	mDeltaV = mDeltaV + mDeltaR*accel*0.005;
	mDeltaP = mDeltaP + ((mDeltaV*0.005) + (0.5*mDeltaR*accel*0.005*0.005));

	//cv::Mat gyroJ(3,1,CV_64F); cv::Mat accelJ(3,1,CV_64F); cv::Mat Rik(3,3,CV_64F);

	// TODO: 1)Biases from previous Keyframe. 2) Jr real code
	//Eucl_bias = math::sqrt(bx^2 + by^2 + bz^2) //biases of previous keyframe
	//Jr = eye(3,3) - convertToSkemM(((1-cos(Eucl_bias))/(Eucl_bias^2))*mBg) + convertToSkewM(((Eucl_bias - sin(Eucl_bias))/(Eucl_bias^3))*(mBg^2)); //mBg biases of prev keyframe in skew symmetric form
	
	//gyroJ = gyro - mBgKeyframe;
	//accelJ = gyro - mBaKeyframe;

	//Rik = convertToSkewM(gyroJ);

	// Jacobians
	cv::Mat Jr(3,3,CV_64F);
	Jr = computeJr(mBg);
	//cout << Jr << endl;
	mJRg = mJRg + (Rik.t()*Jr*0.005);
	mJVg = mJVg + (Rik*convertToSkewM(accelP)*mJRg*0.005);
	mJVa = mJVa + (Rik*0.005);
	mJPg = mJPg + ((mJVg*0.005)-(0.5*Rik*convertToSkewM(accelP)*mJRg*0.005*0.005));
	mJPa = mJPa + ((mJVa*0.005)-(0.5*Rik*0.005*0.005));
	//cout << "im here" << endl;

}
void IMUFrame::IntegrateFrame()
{
	timesincelastKf = lastTimestamp -mTimeStamp;
	timesincelastKf = timesincelastKf/1000000000;

	mRwb = mRwb*mDeltaR*Exp(-mJRg*mBg);
	//cout << mJRg << endl;
	mVwb = mVwb + (Gw*timesincelastKf) + mRwb*(mDeltaV - (mJVg*mBg) - (mJVa*mBa));
	mPwb = mPwb + (mVwb*timesincelastKf) + (0.5*Gw*timesincelastKf*timesincelastKf) + mRwb*(mDeltaP + (mJPg*mBg) + (mJPa*mBa));

	cv::Mat mat(4,4,CV_64F);
	mat = cv::Mat::eye(4,4,CV_64F);
	mRwb.copyTo(mat.rowRange(0,3).colRange(0,3));
	mPwb.copyTo(mat.rowRange(0,3).col(3));
	mat.copyTo(mTwb);
	//cout <<  "im here" << endl;
	//cout << timesincelastKf << endl;
	//cout << mTwb << endl;
	//Rbw = Rwb.t();

}
void IMUFrame::IntegrateKeyFrame()
{
	timesincelastKf = lastTimestamp -mTimeStamp;
	


	mRwb = Rwb_i*mDeltaR*Exp(-mJRg*mBg);
	mVwb = Vwb_i + (Gw*timesincelastKf) + Rwb_i*(mDeltaV - (mJVg*mBg) - (mJVa*mBa));
	mPwb = Pwb_i + (Vwb_i*timesincelastKf) + (0.5*Gw*timesincelastKf*timesincelastKf) + Rwb_i*(mDeltaP + (mJPg*mBg) + (mJPa*mBa));
}
void IMUFrame::UpdateCovariance(const double &ax, const double &ay, const double &az, const double &gx, const double &gy, const double &gz)
{
	cv::Mat MatrixA(9,9,CV_64F);
	cv::Mat MatrixB(9,6,CV_64F);
	
	cv::Mat R11(3,3,CV_64F), R21(3,3,CV_64F), R31(3,3,CV_64F), R23(6,6,CV_64F), R13(9,6,CV_64F), R32(3,3,CV_64F), BR1(3,3,CV_64F), BR2(3,3,CV_64F), BJr(3,3,CV_64F);

	cv::Mat gyro(3,1,CV_64F); cv::Mat accel(3,1,CV_64F); cv::Mat Rik(3,3,CV_64F);
	cv::Mat gyroP(3,1,CV_64F); cv::Mat accelP(3,1,CV_64F);

	gyro.at<double>(0,0) = gx; gyro.at<double>(1,0) = gy; gyro.at<double>(2,0) = gz;
	accel.at<double>(0,0) = ax; accel.at<double>(1,0) = ay; accel.at<double>(2,0) = az;

	timesincelastKf = lastTimestamp -mTimeStamp;
	//cout << gyro << endl;
	//cout << accel << endl;
	//cout << mDeltaR << endl;
	// Subtract bias
	gyroP = gyro - mBg;
	accelP = accel - mBa;
	//cout << "1st calc" << endl;

	R11 = Exp(gyroP)*0.005;
	R21 = -mDeltaR*convertToSkewM(accelP)*0.005;
	R31 = -0.5*mDeltaR*convertToSkewM(accelP)*(0.005*0.005);
	R13 = cv::Mat::zeros(9,6,CV_64F);
	R23 = cv::Mat::eye(6,6,CV_64F);
	R32 = 0.005*cv::Mat::eye(3,3,CV_64F);
	//cout << R11 << endl;
	//cout << R21 << endl;
	//cout << "2nd Calc" << endl;
	BR1 = mDeltaR*0.005;
	BR2 = 0.5*mDeltaR*0.005*0.005;
	BJr = computeJr(gyroP)*0.005;
	//double Eucl_bias = math::sqrt(gyroPx^2 + gyroPy^2 + gyroPz^2) //biases of previous keyframe
	//BJr = eye(3,3) - convertToSkemM(((1-cos(Eucl_bias))/(Eucl_bias^2))*gyroP) + convertToSkewM(((Eucl_bias - sin(Eucl_bias))/(Eucl_bias^3))*(gyroP^2));
    //cout << "3rd calc" << endl;
    MatrixA = cv::Mat::zeros(9,9,CV_64F);
    MatrixB = cv::Mat::zeros(9,6,CV_64F);
	R11.copyTo(MatrixA.rowRange(0,3).colRange(0,3));
	R21.copyTo(MatrixA.rowRange(3,6).colRange(0,3));
	R31.copyTo(MatrixA.rowRange(6,9).colRange(0,3));
	R13.copyTo(MatrixA.rowRange(0,9).colRange(3,9));
	R23.copyTo(MatrixA.rowRange(3,9).colRange(3,9));
	R32.copyTo(MatrixA.rowRange(6,9).colRange(3,6));

	BJr.copyTo(MatrixB.rowRange(0,3).colRange(0,3));

	BR1.copyTo(MatrixB.rowRange(3,6).colRange(3,6));
	BR2.copyTo(MatrixB.rowRange(6,9).colRange(3,6));
	//cout << "Passed copy" << endl;
	//cout << MatrixA << endl;
	//cout << MatrixB << endl;

	cv::add((MatrixA*mEij)*(MatrixA.t()),(MatrixB*NoiseCovIMU)*(MatrixB.t()),mEij);
	//cout << mEij << endl;
	//cout << "Last Calc" << endl;
}
void IMUFrame::computeInformationMatrix()
{	
	E_I = mEij.t();
	//cout <<  "im here" << endl;
}
void IMUFrame::computeCamPose()
{
	
}

void IMUFrame::ResetJandPI()
{
	mJRg = cv::Mat::zeros(3,3,CV_64F);
	mJVg = cv::Mat::zeros(3,3,CV_64F);
    mJPg = cv::Mat::zeros(3,3,CV_64F);
    mJVa = cv::Mat::zeros(3,3,CV_64F);
	mJPa = cv::Mat::zeros(3,3,CV_64F);
	mDeltaR = cv::Mat::eye(3,3,CV_64F);
	mDeltaV = cv::Mat::zeros(3,1,CV_64F);
	mDeltaP = cv::Mat::zeros(3,1,CV_64F);
}
cv::Mat IMUFrame::convertToSkewM(cv::Mat mVector)
{
	cv::Mat mat(3,3,CV_64F);
	double v1,v2,v3;
	v1 = mVector.at<double>(0,0);
	v2 = mVector.at<double>(1,0);
	v3 = mVector.at<double>(2,0);
	//double zero = 0.0;
	mat = cv::Mat::zeros(3,3,CV_64F);

	/*mat.at<double>(0,0) = zero;*/ mat.at<double>(0,1) = -v3; mat.at<double>(0,2) = v2;
	mat.at<double>(1,0) = v3; /*mat.at<double>(0,1) = zero;*/ mat.at<double>(1,2) = -v1;
	mat.at<double>(2,0) = -v2; mat.at<double>(2,1) = v1; /*mat.at<double>(0,2) = zero;*/

	return mat;
}
cv::Mat IMUFrame::Exp(cv::Mat R)
{
	cv::Mat I(3,3,CV_64F); cv::Mat res(3,3,CV_64F); cv::Mat R2(3,1,CV_64F); cv::Mat R1(3,1,CV_64F);
	if(R.cols>1)
	{
		R1.row(0) = R.row(2).col(1);
		R1.row(1) = R.row(0).col(2);
		R1.row(2) = R.row(1).col(0);
	}
	else
	{
		R1 = R.clone();
	}
	
	I = cv::Mat::eye(3,3,CV_64F);

	double v1,v2,v3,norm;
	v1 = R1.at<double>(0,0);
	v2 = R1.at<double>(1,0);
	v3 = R1.at<double>(2,0);

	norm = sqrt(pow(v1,2) + pow(v2,2) + pow(v3,2)); 
	cv::pow(R1,2,R2);

	//cout << norm << endl;
	//cout << convertToSkewM(R1) << endl;
	//cout << convertToSkewM(R2) << endl;
	if(norm<0.00001)
	{
		res = I + convertToSkewM(R1) + convertToSkewM(R2);
	}
	else
	{
		res = I + (sin(norm)/norm)*convertToSkewM(R1) + ((1 - cos(norm))/pow(norm,2))*convertToSkewM(R2);
	}
	
	//cout << res << endl;
	return res;
}
cv::Mat IMUFrame::computeJr(cv::Mat B)
{
	cv::Mat I(3,3,CV_64F); cv::Mat res(3,3,CV_64F); cv::Mat R2(3,1,CV_64F); cv::Mat R1(3,1,CV_64F);
	if(B.cols>1)
	{
		R1.row(0) = B.row(2).col(1);
		R1.row(1) = B.row(0).col(2);
		R1.row(2) = B.row(1).col(0);
	}
	else
	{
		R1 = B.clone();
	}
	
	I = cv::Mat::eye(3,3,CV_64F);
	double v1,v2,v3,norm;
	v1 = R1.at<double>(0,0);
	v2 = R1.at<double>(1,0);
	v3 = R1.at<double>(2,0);
	norm = sqrt(pow(v1,2) + pow(v2,2) + pow(v3,2)); 
	cv::pow(R1,2,R2);
	//cout << norm << endl;
	//cout << convertToSkewM(R1) << endl;
	//cout << convertToSkewM(R2) << endl;

	if(norm<0.00001)
	{
		res = I + convertToSkewM(R1) + convertToSkewM(R2);
	}
	else
	{
		res = I - ((1 - cos(norm))/pow(norm,2))*convertToSkewM(R1) + ((norm - sin(norm))/pow(norm,3))*convertToSkewM(R2);
	}
	

	return res;
}
};