#ifndef IMUFRAME_H
#define IMUFRAME_H

#include<vector>
#include <opencv2/opencv.hpp>
#include "KeyFrame.h"
namespace ORB_SLAM2
{

class KeyFrame;


class IMUFrame
{
public:
   IMUFrame();
   
   //TODO: Include all variables in copy segment
   //copy constructor
   IMUFrame(const IMUFrame &Imuframe);

   //TODO: Include all variables in copy segment, Include logic functions for Covariance update
   //Constructor for IMU data
   IMUFrame(KeyFrame* LastKF,const double &ax, const double &ay, const double &az, const double &gx, const double &gy, const double &gz, const double &timestamp,const bool &IntRdy,const cv::Mat &JRg, const cv::Mat &JVg, const cv::Mat &JVa, const cv::Mat &JPg, const cv::Mat &JPa, const cv::Mat &DeltaR, const cv::Mat &DeltaV,const cv::Mat &DeltaP, const cv::Mat &Eij);
   
   //Set Cam Pose
   void SetCamPose(cv::Mat Tcw);

   void setReferenceKF(KeyFrame* mKF);
   //Set IMU Position,velocity,Rotation and biases
   void SetState(cv::Mat Rwb, cv::Mat Vwb, cv::Mat Pwb);

   // Update Jacobians and preintegrations
   void UpdateJandPI(const double &ax, const double &ay,const double &az,const double &gx,const double &gy, const double &gz);

   // Integration for incoming Frame
   void IntegrateFrame();

   //TODO: Implement Exponential Map and Logarithm Map for SO3
   // Integration for consecutive Keyframes
   void IntegrateKeyFrame();

   // Covariance Update
   void UpdateCovariance(const double &ax, const double &ay, const double &az, const double &gx, const double &gy, const double &gz);

   // Create Information Matrix for Optimization
   void computeInformationMatrix();
   
   // Reset Jacobians and Preintegrations	 
   void ResetJandPI();

   cv::Mat Exp(cv::Mat R);

   cv::Mat computeJr(cv::Mat B); 

   // Convert to skew symmetric matrix
   cv::Mat convertToSkewM(cv::Mat mVector);

   // Returns the camera center.
   inline cv::Mat GetCameraCenter(){
        return mOw.clone();
   }

   //TODO: Include get function for Information Matrix

   // Returns inverse of camera rotation
   inline cv::Mat GetCameraRotation(){
        return mRwc.clone();
   }
   inline cv::Mat GetPreIntInformation(){
        return E_I.clone();
   }
   inline cv::Mat GetBiasInformation(){
         return NoiseCovIMU.clone();
   }
   inline cv::Mat GetIMUPose(){
         return mTwb.clone();
   }
   // Returns IMU rotation
   inline cv::Mat GetIMURotation(){
	return mRwb.clone();
   }
   // Returns IMU velocity
   inline cv::Mat GetIMUVelocity(){
        return mVwb.clone();
   }
   // Return IMU position
   inline cv::Mat GetIMUPosition(){
	return mPwb.clone();
   }
   // Returns gyro bias
   inline cv::Mat GetGyroBias(){
	return mBg.clone();
   }
   // Returns accel bias
   inline cv::Mat GetAccelBias(){
	return mBa.clone();
   }
public:

   // Frame Timestamp
   double mTimeStamp;
   double lastTimestamp;
   double timesincelastKf;

   // Flag for Full Integration
   bool mIntRdy;

   // Flag for new keyframe
   bool mnewKeyFrame;

   cv::Mat RikG;

   cv::Mat mEij;
   cv::Mat E_I;

   cv::Mat NoiseCovIMU;

   // Accel and gyro terms
   // Jacobians terms for rotation, velocity and position
   cv::Mat mJRg, mJVg, mJVa, mJPg, mJPa;

   // Preintegration terms for rotation, velocity and position
   cv::Mat mDeltaR, mDeltaV, mDeltaP;

   // IMU Rotation, translation, position
   cv::Mat mRwb, mRbw;
   cv::Mat mVwb, mVbw; 
   cv::Mat mPwb, mPbw;

   // Last KF rotation, translation and position
   cv::Mat Rwb_i, Vwb_i, Pwb_i;

   // IMU biases
   cv::Mat mBg, mBa;

   // Camera Pose transformation
   cv::Mat mTcw;

   // IMU Pose transformation
   cv::Mat mTwb;

   cv::Mat Gw;

   // Last Frame and KeyFrame
  // Frame* mLF;
   KeyFrame* mLKF;

   // Current and Next Frame id.
   static long unsigned int nNextId;
   long unsigned int mnID;


private:
   // Compute Camera pose transformation from IMU position and rotation
   void computeCamPose();

   // Camera Rotation, translation and center
   cv::Mat mRcw;
   cv::Mat mtcw;
   cv::Mat mRwc;
   cv::Mat mOw; //==mtwc

};
} //namespace 
#endif
